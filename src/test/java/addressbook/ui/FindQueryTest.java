package addressbook.ui;

import org.testng.Assert;
import org.testng.annotations.Test;

import addressbook.ui.FindQuery;
import addressbook.ui.Query;

public class FindQueryTest {	
	@Test(groups = { "polymorphic" })
	public void testPolymorphic() {
		final Query qry = Query.create("find John");
		Assert.assertEquals(qry.getClass(), FindQuery.class);
	}

	/**
	 * This method tests whether we can correctly parse the queried name.
	 */
	@Test(dependsOnGroups = { "polymorphic" }, groups = { "functional" })
	public void testParse() {
		final Query qry = Query.create("find John");
		Assert.assertEquals(((FindQuery) qry).parseName(), "John");
	}
}
