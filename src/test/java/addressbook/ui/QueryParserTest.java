package addressbook.ui;

import java.util.Collection;
import java.util.LinkedList;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import addressbook.domain.Name;

public class QueryParserTest {

	private String qryAdd, qryFind;

	@BeforeTest(alwaysRun = true)
	public void setUp() {
		qryAdd = "add John, Doe1, 503-555-1212, 503-555-1234, jdoe@mailinator.com, john@mailinator.com";
		qryFind = "find John";
	}
	
	@Test
	public void testParseNameAdd()
	{
		Name expected;
		Name actual;
		
		actual = new QueryParser(qryAdd).parseNameAdd();
		expected = new Name("John", "Doe1");
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void testParseNameFind()
	{
		final Collection<String> expected, actual;
		
		actual = new QueryParser(qryFind).parseNameFind();
		expected = new LinkedList<String>();
		expected.add("John");
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void testParseEmailAdd() {
		final Collection<String> expected, actual;
		
		actual = new QueryParser(qryAdd).parseEmail();
		expected = new LinkedList<String>();
		expected.add("jdoe@mailinator.com"); expected.add("john@mailinator.com");
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void testParsePhone() {
		final Collection<String> expected, actual;
		
		actual = new QueryParser(qryAdd).parsePhoneNo();
		expected = new LinkedList<String>();
		expected.add("503-555-1212"); expected.add("503-555-1234");
		
		Assert.assertEquals(actual, expected);
	}
}
