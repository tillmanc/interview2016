package addressbook.domain;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PersonTest {

	private Person john, john2, notjohnname, notjohnemail, notjohnphone, sally;
	
	@BeforeTest(alwaysRun=true)
	public void setUpPersons()
	{
		final String johnemail1, johnemail2, johnphone1, johnphone2;
		
		johnemail1 = "john.doe1@mailinator.com";
		johnemail2 = "john.doe2@mailinator.com";
		johnphone1 = "855-433-2211";
		johnphone2 = "111-111-1111";
		
		john = new Person(new Name("John", "Smith"));
		john.addEmail(johnemail1);
		john.addPhoneNumber(johnphone1);

		john2 = new Person(new Name("John", "Smith"));
		john2.addEmail(johnemail1);
		john2.addPhoneNumber(johnphone1);
		
		notjohnname = new Person(new Name("John", "Sith"));
		notjohnname.addEmail(johnemail1);
		notjohnname.addPhoneNumber(johnphone2);
		
		notjohnemail = new Person(new Name("John", "Smith"));
		notjohnemail.addEmail(johnemail1);
		notjohnemail.addEmail(johnemail2);
		notjohnemail.addPhoneNumber(johnphone1);
		
		notjohnphone = new Person(new Name("John", "Smith"));
		notjohnphone.addEmail(johnemail1);
		notjohnphone.addPhoneNumber(johnphone2);

		sally = new Person(new Name("Sally", "Seashore"));	
	}
	
	@Test(groups = { "equivalance" })
	public void testEquivalence() {
		Assert.assertTrue(john.equals(john2) && john2.equals(john), "symmetry");
		Assert.assertEquals(john, john, "reflexivity");

		Assert.assertNotEquals(john, sally);
		Assert.assertNotEquals(john2, sally);
	}

	@Test(groups = { "equivalence" })
	public void testSameFields() {
		Assert.assertNotEquals(john, notjohnname);
		Assert.assertNotEquals(john, notjohnemail);
		Assert.assertNotEquals(john, notjohnphone);
	}
}
