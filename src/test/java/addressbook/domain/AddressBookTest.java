package addressbook.domain;

import java.util.Collection;
import java.util.LinkedList;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AddressBookTest {
	private Person johndoe, johndeere, dearjohn;

	@BeforeTest(alwaysRun = true)
	public void setUp() {
		johndoe = new Person(new Name("John", "Doe"));
		johndeere = new Person(new Name("John", "Deere"));
		dearjohn = new Person(new Name("Dear", "John"));
	}

	@Test
	public void find() {
		final AddressBook addressBook = new AddressBook();
		final Collection<Person> actual, expected;
		
		addressBook.add(johndoe);
		addressBook.add(johndeere);
		addressBook.add(dearjohn);
		
		actual = addressBook.find("John");
		expected = new LinkedList<Person>();
		
		expected.add(johndoe);
		expected.add(dearjohn);
		expected.add(johndeere);
		
		Assert.assertEquals(actual, expected);
	}
}
