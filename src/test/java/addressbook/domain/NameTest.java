package addressbook.domain;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NameTest {

  @Test
  public void equals() {
    final Name john, notjohn, stilljohn;
    
    john = new Name("John", "Smith");
    notjohn = new Name("John", "Sally");
    stilljohn = new Name("John", "Smith");
    
    Assert.assertTrue(john.equals(stilljohn) && stilljohn.equals(john));
    Assert.assertNotEquals(john, notjohn);
  }
}
