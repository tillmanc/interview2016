package addressbook.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import addressbook.domain.Name;

public class QueryParser {
	public static final String EMAIL_RGX = "\\b([A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,})\\b",
			PHONENO_RGX = "\\b([0-9]{3}-[0-9]{3}-[0-9]{4})\\b"
			,ADD_NAME_RGX = " ([A-Z0-9]{1,}),[ ]([A-Z0-9]{1,})\\b"
			,FIND_NAME_RGX = " ([A-Z0-9]{1,})\\b";

	private final String query;

	public QueryParser(final String query) {
		this.query = query;
	}

	private Collection<String> parse_query(final String rgx) {
		final Pattern pattern;
		final Matcher matcher;
		final Collection<String> match_groups;
		pattern = Pattern.compile(rgx, Pattern.CASE_INSENSITIVE);
		matcher = pattern.matcher(query);
		match_groups = new LinkedList<String>();

		while (matcher.find())
		{
			for (int i = 1; i <= matcher.groupCount(); i++)
				match_groups.add(matcher.group(i));
		}

		return match_groups;
	}
	
	public Name parseNameAdd()
	{
		ArrayList<String> names =  new ArrayList<String>(parse_query(ADD_NAME_RGX));
		
		if (names.size() != 2) {
			throw new RuntimeException("Query must have first and last name");
		}
		
		return new Name(names.get(0), names.get(1));
	}
	
	public Collection<String> parseNameFind()
	{
		return parse_query(FIND_NAME_RGX);
	}
	
	public Collection<String> parseEmail()
	{
		return parse_query(EMAIL_RGX);
	}
	
	public Collection<String> parsePhoneNo() {
		return parse_query(PHONENO_RGX);
	}
}
