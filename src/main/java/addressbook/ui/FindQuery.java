package addressbook.ui;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import addressbook.domain.AddressBook;
import addressbook.domain.Person;

class FindQuery extends Query {
	
	public FindQuery(final String command)
	{
		super(command);
	}
	
	@Override
	public Collection<Person> perform(AddressBook addressbook) {
		Collection<Person> found = addressbook.find(parseName());
		
		found.forEach((Person P) -> System.out.println(P.getName()));
		
		return found;
	}

	public String parseName()
	{
		final Pattern pattern = Pattern.compile("^find (\\w+)$");
		final Matcher matcher = pattern.matcher(getCommand());
		
		if (matcher.find()) {
			return matcher.group(1);
		} else {
			throw new IllegalArgumentException("Find query must have argument.");
		}
	}
}
