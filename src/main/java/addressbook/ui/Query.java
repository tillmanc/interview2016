package addressbook.ui;

import java.util.Collection;
import java.util.regex.Pattern;

import addressbook.domain.AddressBook;
import addressbook.domain.Person;

public abstract class Query {
	private static final String FIND_RGX = "(?i)^find\\b";
	private static final String ADD_RGX = "(?i)^add\\b";
	
	private final String command;
	
	public static Query create(final String command)
	{	
		if (Pattern.compile(FIND_RGX).matcher(command).find()) {
			return new FindQuery(command);
		} else if ((Pattern.compile(ADD_RGX).matcher(command).find())) {
			return new AddQuery(command);
		} else {
			throw new IllegalArgumentException("Invalid Command");
		}
	}
	
	protected Query(final String command) {
		this.command = command;
	}
	
	public String getCommand() {
		return command;
	}
	
	public abstract Collection<Person> perform(AddressBook addressbook);
}
