package addressbook.ui;

import java.util.ArrayList;
import java.util.Collection;

import addressbook.domain.AddressBook;
import addressbook.domain.Name;
import addressbook.domain.Person;

class AddQuery extends Query {
	public AddQuery(final String command) {
		super(command);
	}

	@Override
	public Collection<Person> perform(AddressBook addressbook) {
		final QueryParser qp = new QueryParser(getCommand());
		final Name name = qp.parseNameAdd();
		final Collection<String> email = qp.parseEmail(), phone = qp.parsePhoneNo();
		final ArrayList<Person> persons = new ArrayList<Person>();

		final Person added = new Person(name);
		email.stream().forEach((String s) -> added.addEmail(s));
		phone.stream().forEach((String s) -> added.addPhoneNumber(s));

		addressbook.add(added);
		persons.add(added);
		System.out.println("added " + name);
		
		return persons;
	}
}
