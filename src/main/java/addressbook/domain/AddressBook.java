package addressbook.domain;

import java.util.Collection;
import java.util.TreeSet;
import java.util.stream.Collectors;

//TODO what about duplicates? give error on duplicate
public final class AddressBook {
	private final Collection<Person> addressBook;

	private final static String NAME_IS_NULL = "Name cannot be null.";

	public AddressBook() {
		addressBook = new TreeSet<Person>();
	}

	public boolean add(final Person person) {
		if (null == person) {
			throw new IllegalArgumentException("person cannot be null");
		}
		
		if (addressBook.stream().map((Person p) -> p.getName()).anyMatch((Name x) -> x.equals(person.getName())))
		{
			throw new RuntimeException("Illegal input: Person already exists");
		}

		return addressBook.add(person);
	}

	public Collection<Person> find(final String name) {
		if (null == name) {
			throw new IllegalArgumentException(NAME_IS_NULL);
		}

		return addressBook.stream()
				.filter((Person P) -> P.getName().getFirstName().equals(name) || P.getName().getLastName().equals(name))
				.collect(Collectors.toSet());
	}

}
