package addressbook.domain;

import java.util.Collection;
import java.util.TreeSet;
import java.util.function.Function;

public final class Person implements Comparable<Person> {
	private Name name;
	private Collection<String> emails;
	private Collection<String> phoneNumbers;
	
	private static final String NAME_IS_NULL = "Name cannot be null";
	
	private static final String EMAIL_IS_NULL = "Email cannot be null";
	private static final String EMAIL_IS_EMPTY = "Email cannot be empty";
	
	private static final String PHONE_IS_NULL = "Phone number cannot be null";
	private static final String PHONE_IS_EMPTY = "Phone number cannot be empty";

	public Person(final Name name) {
		if (null == name) {
			throw new IllegalArgumentException(NAME_IS_NULL);
		} 
		this.name = name;
		this.emails = new TreeSet<String>();
		this.phoneNumbers = new TreeSet<String>();
	}

	public void changeNameTo(final Name name) {
		this.name = name;
	}

	public Name getName() {
		return this.name;
	}

	public Collection<String> getEmails() {
		return emails;
	}

	public boolean addEmail(final String email) {
		if (null == email) {
			throw new IllegalArgumentException(EMAIL_IS_NULL);
		}
		if (email.isEmpty()) {
			throw new IllegalArgumentException(EMAIL_IS_EMPTY);
		}
		
		return this.emails.add(email);
	}

	public boolean addPhoneNumber(final String phoneNo) {
		if (null == phoneNo) {
			throw new IllegalArgumentException(PHONE_IS_NULL);
		}
		if (phoneNo.isEmpty()) {
			throw new IllegalArgumentException(PHONE_IS_EMPTY);
		}
		return this.phoneNumbers.add(phoneNo);
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((emails == null) ? 0 : emails.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phoneNumbers == null) ? 0 : phoneNumbers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (emails == null) {
			if (other.emails != null)
				return false;
		} else if (!emails.equals(other.emails))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phoneNumbers == null) {
			if (other.phoneNumbers != null)
				return false;
		} else if (!phoneNumbers.equals(other.phoneNumbers))
			return false;
		return true;
	}

	public int compareTo(Person o) {
		assert (o != null);
		final int nameCmpr, phoneCmpr, emailCmpr;

		final Function<Collection<String>, Integer> sumHash = (Collection<String> c) -> c.stream().map(String::hashCode)
				.reduce(0, (a, b) -> a + b);

		nameCmpr = this.name.compareTo(o.name);
		phoneCmpr = sumHash.apply(phoneNumbers) - sumHash.apply(o.phoneNumbers);
		emailCmpr = sumHash.apply(emails) - sumHash.apply(o.emails);

		return nameCmpr + phoneCmpr + emailCmpr;
	}

}
