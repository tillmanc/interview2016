package addressbook;

import java.util.NoSuchElementException;
import java.util.Scanner;
import addressbook.domain.AddressBook;
import addressbook.ui.Query;

public class App {
	public static void main(String[] args) {
		final Scanner scanner = new Scanner(System.in);
		final AddressBook addresses = new AddressBook();

		String input;
		Query query;
		while (true) {
			try {
				System.out.print("> ");
				input = scanner.nextLine();
				query = Query.create(input);
				
				query.perform(addresses);
			} catch (NoSuchElementException ex) {
				System.out.println("Goodbye.");
				scanner.close();
				System.exit(0);
			} catch(RuntimeException e) {
				System.out.println(e.getMessage());
			}
		}

	}
}
